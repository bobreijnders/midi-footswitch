/*
 * MIDI Foot switch for PC via USB 
 * To switch rigs, effects or gain channels in Digital Guitar Amps like ReValver4, Guitar Rig, etc.
 *
 * Documentation about MIDI protocol https://tttapa.github.io/Arduino/MIDI/Chap01-MIDI-Protocol.html
 * Has al the midiEventPacket_t channel specs etc.
 * 
 * This example contains 3 buttons and 2 modes
 * Modes can be changed by longpressing a button, blink will let you know the mode has changed
 * 
 * Author: Bob Reijnders
 */ 

#include "MIDIUSB.h"

// LED
#define LED_PIN 3

// BUTTONS | pins
#define BUTTON_LEFT   9         // pin number of left button
#define BUTTON_DOWN   16        // pin number of bottom or centre button
#define BUTTON_RIGHT  10        // pin number or right button

// BUTTONS | debounce, press and release
#define LONGPRESS_DELAY 1000    // miliseconds till a long press is registered
#define DEBOUNCE_DELAY 100      // miliseconds till new input is accepted
unsigned long lastDebounceTime = 0;       // last millis() time a button was pressed
int buttonPressed = 0;          // pin number of dominant button pressed
int lastButtonPressed = 0;      // pin number of last dominant button pressed

// PEDAL | mode
#define MODE_TWIN 0             // Program A, Switch Rig 1/2  , Program B
#define MODE_FULL 1             // Prev Rig , Reset           , Next Rig
byte pedalMode = MODE_TWIN;

// MIDI | control change MODE_TWIN
#define CONTROL_A 1
#define CONTROL_B 2 
byte controlFlagA = 0;          // 0 = next control change sets control channel A to MAX value 
byte controlFlagB = 0;          // 1 = next control change sets control channel B to MIN value 

// MIDI | program control MODE_FULL
#define PROGRAM_MAX 8
uint8_t programIndex = 0;

void setup() {
  // for debug purposes
  Serial.begin(115200);

  // init Buttons
  pinMode(BUTTON_LEFT, INPUT_PULLUP);
  pinMode(BUTTON_DOWN, INPUT_PULLUP);
  pinMode(BUTTON_RIGHT, INPUT_PULLUP);

  // init LED
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);
}

void loop() {

  // time since last key press
  unsigned long debounceTime = millis() - lastDebounceTime;

  // Pedal Ready to receive input
  if(debounceTime > DEBOUNCE_DELAY){

    // read button states
    buttonPressed = 0;
    buttonPressed = !digitalRead(BUTTON_LEFT) ? BUTTON_LEFT : buttonPressed;
    buttonPressed = !digitalRead(BUTTON_DOWN) ? BUTTON_DOWN : buttonPressed;
    buttonPressed = !digitalRead(BUTTON_RIGHT) ? BUTTON_RIGHT : buttonPressed;

    // button pressed
    if(buttonPressed && buttonPressed != lastButtonPressed){
      switch(pedalMode){
        case MODE_TWIN:
          controlerModeTwin(buttonPressed);
          break;
          
        case MODE_FULL:
          controlerModeFull(buttonPressed);
          break;
      }

      // debouncer
      lastDebounceTime = millis();
    }

    // button longpressed - start blinking to indicate imminent pedal mode change
    if(buttonPressed && debounceTime > LONGPRESS_DELAY && buttonPressed == lastButtonPressed){
      // blink LED
      digitalWrite(LED_PIN, !digitalRead(LED_PIN));
    }

    // button released - possible mode change
    if(!buttonPressed && buttonPressed != lastButtonPressed){
      
      // change pedal mode when longpressed
      if(debounceTime > LONGPRESS_DELAY){
        // switch
        pedalMode = pedalMode ? MODE_TWIN : MODE_FULL;
  
        // re-init on mode change
        programIndex = 0;

        // count this as a key press
        lastDebounceTime = millis();

        // reset LED
        digitalWrite(LED_PIN, HIGH);

        Serial.print("Mode: ");
        Serial.println(pedalMode);
      }
    }
    
    // button state change
    if(buttonPressed != lastButtonPressed) lastButtonPressed = buttonPressed;
    

  } 

  // speed limiter
  delay(50); 
}


// Function controlerModeTwin
// pedal behaviour: rig toggle and two effect switches
void controlerModeTwin(int buttonPressed){
  
  switch (buttonPressed){
    case BUTTON_LEFT: // Effect A
      controlFlagA = controlFlagA ? 0 : 1;                  // toggle flag
      controlChange(0, CONTROL_A, (controlFlagA * 255));    // channel, control, value
      Serial.print("Control change: ");
      Serial.print(CONTROL_A);
      Serial.print(" to value ");
      Serial.println((controlFlagA * 255));
      break;
  
    case BUTTON_DOWN: // Switch Rig
      programIndex = programIndex ? 0 : 1;                  // switch programIndex 
      programChange(0, programIndex);                       // channel, programIndex
      digitalWrite(LED_PIN, !programIndex);                 // switch LED on or off based on selected program
      Serial.print("Program change to: ");                  // print new state
      Serial.println(programIndex);
      break;  
      
    case BUTTON_RIGHT: // Effect B
      controlFlagB = controlFlagB ? 0 : 1;                  // toggle flag
      controlChange(0, CONTROL_B, (controlFlagB * 255));    // channel, control, value
      Serial.print("Control change: ");                     // print new state
      Serial.print(CONTROL_B);
      Serial.print(" to value ");
      Serial.println((controlFlagB * 255));
      break;  
  }
  
  // send to PC
  MidiUSB.flush();
}

// PEDAL MODE | Full
// pedal behaviour: 8 rigs selectable using previous and next buttons. The down button resets pedal to first rig
void controlerModeFull(int buttonPressed){
  
  switch (buttonPressed){
    case BUTTON_LEFT: // previous rig
      if (programIndex == 0) {
        programIndex = PROGRAM_MAX;
      } else {
        programIndex--;
      }
      programChange(0, programIndex);         // channel, programIndex
      break;
  
    case BUTTON_DOWN: // reset to first rig
      programIndex = 0;
      programChange(0, programIndex);     // channel, programIndex
      break;  
      
    case BUTTON_RIGHT: // next rig
      programIndex++;
      if ( programIndex >= PROGRAM_MAX) programIndex = 0;
      programChange(0, programIndex);         // channel, programIndex
      break;  
  }
  
  // send to PC
  MidiUSB.flush();

  // print new state
  Serial.print("Program change to: ");
  Serial.println(programIndex);
}

// MIDI Program Change
// @param channel - MIDI event type (0x0C = program change)
// @param control - switch to preset number {0 - 7 for Revalver4 use} 
void programChange(uint8_t channel, uint8_t control) {
  midiEventPacket_t event = {0x0C, 0xC0 | channel, control, 0};
  MidiUSB.sendMIDI(event);
}

// MIDI Control Change
// First parameter is the event type (0x0B = control change).
// Second parameter is the event type, combined with the channel.
// Third parameter is the control number number (0-119).
// Fourth parameter is the control value (0-127).
void controlChange(uint8_t channel, uint8_t control, uint8_t value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}
